from deepface.basemodels import VGGFace, OpenFace, Facenet, Facenet512, FbDeepFace, DeepID, DlibWrapper as Dlib, ArcFace
from deepface.detectors import OpenCvWrapper, SsdWrapper, DlibWrapper, MtcnnWrapper, RetinaFaceWrapper
from deepface.commons.functions import normalize_input, find_input_shape, load_image
from deepface.extendedmodels import Age, Gender, Race, Emotion

from tensorflow.keras.preprocessing import image

from deepface.detectors.FaceDetector import build_model as fd_build_model
import tensorflow as tf
import numpy as np
import warnings
import cv2
import os

tf_version = int(tf.__version__.split(".")[0])
if tf_version == 2:
    import logging

    tf.get_logger().setLevel(logging.ERROR)
warnings.filterwarnings("ignore")

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


def build_model(model_name):
    """
	This function builds a deepface model
	Parameters:
		model_name (string): face recognition or facial attribute model
			VGG-Face, Facenet, OpenFace, DeepFace, DeepID for face recognition
			Age, Gender, Emotion, Race for facial attributes

	Returns:
		built deepface model
	"""

    global model_obj  # singleton design pattern

    models = {
        'VGG-Face': VGGFace.loadModel,
        'OpenFace': OpenFace.loadModel,
        'Facenet': Facenet.loadModel,
        'Facenet512': Facenet512.loadModel,
        'DeepFace': FbDeepFace.loadModel,
        'DeepID': DeepID.loadModel,
        'Dlib': Dlib.loadModel,
        'ArcFace': ArcFace.loadModel,
        'Emotion': Emotion.loadModel,
        'Age': Age.loadModel,
        'Gender': Gender.loadModel,
        'Race': Race.loadModel
    }

    if not "model_obj" in globals():
        model_obj = {}

    if not model_name in model_obj.keys():
        model = models.get(model_name)
        if model:
            model = model()
            model_obj[model_name] = model
        # print(model_name," built")
        else:
            raise ValueError('Invalid model_name passed - {}'.format(model_name))

    return model_obj[model_name]


def represent(img_obj, model_name='VGG-Face', model=None, enforce_detection=True,
              detector_backend='opencv', align=True, normalization='base', return_regions=False):
    """
	This function represents facial images as vectors.

	Parameters:
		img_obj: exact image path, numpy array or based64 encoded images could be passed.

		model_name (string): VGG-Face, Facenet, OpenFace, DeepFace, DeepID, Dlib, ArcFace.

		model: Built deepface model. A face recognition model is built every call of verify function. You can pass
		pre-built face recognition model optionally if you will call verify function several times. Consider to pass
		model if you are going to call represent function in a for loop.

			model = DeepFace.build_model('VGG-Face')

		enforce_detection (boolean): If any face could not be detected in an image, then verify function will return
		exception. Set this to False not to have this exception. This might be convenient for low resolution images.

		detector_backend (string): set face detector backend as retinaface, mtcnn, opencv, ssd or dlib

		normalization (string): normalize the input image before feeding to model

		align (boolean): aligned or not

		return_regions (boolean): return regions or not

	Returns: Represent function returns a multidimensional vector. The number of dimensions is changing based on the
	reference model. E.g. FaceNet returns 128 dimensional vector; VGG-Face returns 2622 dimensional vector.

    """

    # if model is None:
    #     model = build_model(model_name)

    # ---------------------------------

    # decide input shape
    input_shape_x, input_shape_y = find_input_shape(model)

    # detect and align
    imgs, regions = preprocess_faces(img=img_obj,
                                     target_size=(input_shape_y, input_shape_x),
                                     enforce_detection=enforce_detection,
                                     detector_backend=detector_backend,
                                     align=align, return_regions=return_regions)
    embeddings = []
    for img in imgs:
        # ---------------------------------
        # custom normalization

        img = normalize_input(img=img, normalization=normalization)

        # ---------------------------------
        # represent
        embedding = model.predict(img)[0].tolist()
        embeddings.append(embedding)

    if return_regions:
        return embeddings, regions
    else:
        return embeddings


def preprocess_faces(img, target_size=(224, 224), grayscale=False, enforce_detection=True, detector_backend='opencv',
                     return_regions=True, align=True):
    # img might be path, base64 or numpy array. Convert it to numpy whatever it is.
    img = load_image(img)
    base_img = img.copy()

    imgs, regions = detect_faces(img=img, detector_backend=detector_backend, grayscale=grayscale,
                                 enforce_detection=enforce_detection, align=align)

    imgs_res = []
    regions_res = []

    for img, region in zip(imgs, regions):

        # --------------------------
        if img.shape[0] == 0 or img.shape[1] == 0:
            if enforce_detection:
                raise ValueError("Detected face shape is ", img.shape,
                                 ". Consider to set enforce_detection argument to False.")
            else:  # restore base image
                img = base_img.copy()

        # --------------------------

        # post-processing
        if grayscale:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # ---------------------------------------------------
        # resize image to expected shape

        # img = cv2.resize(img, target_size) #resize causes transformation on base image, adding black pixels to resize
        # will not deform the base image

        factor_0 = target_size[0] / img.shape[0]
        factor_1 = target_size[1] / img.shape[1]
        factor = min(factor_0, factor_1)

        dsize = (int(img.shape[1] * factor), int(img.shape[0] * factor))
        img = cv2.resize(img, dsize)

        # Then pad the other side to the target size by adding black pixels
        diff_0 = target_size[0] - img.shape[0]
        diff_1 = target_size[1] - img.shape[1]
        if not grayscale:
            # Put the base image in the middle of the padded image
            img = np.pad(img, ((diff_0 // 2, diff_0 - diff_0 // 2), (diff_1 // 2, diff_1 - diff_1 // 2), (0, 0)),
                         'constant')
        else:
            img = np.pad(img, ((diff_0 // 2, diff_0 - diff_0 // 2), (diff_1 // 2, diff_1 - diff_1 // 2)), 'constant')

        # double check: if target image is not still the same size with target.
        if img.shape[0:2] != target_size:
            img = cv2.resize(img, target_size)

        # ---------------------------------------------------

        # normalizing the image pixels

        img_pixels = image.img_to_array(img)  # what this line doing? must?
        img_pixels = np.expand_dims(img_pixels, axis=0)
        img_pixels /= 255  # normalize input in [0, 1]

        # ---------------------------------------------------

        imgs_res.append(img_pixels)
        regions_res.append(region)

    if return_regions:
        return imgs_res, regions_res
    else:
        return imgs_res


def detect_faces(img, detector_backend='opencv', grayscale=False, enforce_detection=True, align=True):
    img_regions = [0, 0, img.shape[0], img.shape[1]]

    # ----------------------------------------------
    # people would like to skip detection and alignment if they already have pre-processed images
    if detector_backend == 'skip':
        return img, img_regions

    # ----------------------------------------------

    # detector stored in a global variable in FaceDetector object.
    # this call should be completed very fast because it will return found in memory
    # it will not build face detector model in each call (consider for loops)
    face_detector = fd_build_model(detector_backend)

    try:
        detected_faces, img_regions = get_faces(face_detector, detector_backend, img, align)
    except:  # if detected face shape is (0, 0) and alignment cannot be performed, this block will be run
        detected_faces = None

    if detected_faces is None:
        if not enforce_detection:
            return [img], [img_regions]
        else:
            raise ValueError(
                "Face could not be detected. Please confirm that the picture is a face photo or consider to set "
                "enforce_detection param to False.")
    else:
        if all(isinstance(item, np.ndarray) for item in detected_faces):
            return detected_faces, img_regions


def get_faces(face_detector, detector_backend, img, align=True):
    obj = detect_faces_final(face_detector, detector_backend, img, align)

    faces = [item[0] for item in obj]
    regions = [item[1] for item in obj]

    if len(faces) == 0:
        faces = None
        regions = [0, 0, img.shape[0], img.shape[1]]

    return faces, regions


def detect_faces_final(face_detector, detector_backend, img, align=True):
    backends = {
        'opencv': OpenCvWrapper.detect_face,
        'ssd': SsdWrapper.detect_face,
        'dlib': DlibWrapper.detect_face,
        'mtcnn': MtcnnWrapper.detect_face,
        'retinaface': RetinaFaceWrapper.detect_face
    }

    detect_face = backends.get(detector_backend)
    if detect_face:
        obj = detect_face(face_detector, img, align)
        return obj
    else:
        raise ValueError("invalid detector_backend passed - " + detector_backend)
