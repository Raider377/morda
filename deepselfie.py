from deepface.commons import distance as dst
from os.path import join
from tqdm import tqdm


import numpy as np
import deepfaces
import shutil
import json
import os


def check_exist_path(path):
    if not os.path.exists(path):
        os.mkdir(path)


class DeepSelfie(object):
    def __init__(self, args):
        self.args = args
        self.photos_path = args.photos
        self.selfie_path = args.selfie
        self.json_path = args.json
        self.result_path = args.result
        self.model_name = "Facenet"
        self.model = deepfaces.build_model(self.model_name)

    @staticmethod
    def save_json(data, json_path):
        """
        Save json file to disk
        :param data: json file for saving
        :param json_path: saving name
        :return: pass
        """
        with open(json_path, 'w') as file:
            json.dump(data, file, ensure_ascii=False)

    @staticmethod
    def read_json(json_path):
        """
        Read json file from disk
        :param json_path: name (included dir) for reading file
        :return: loaded json
        """
        with open(json_path, "r") as file:
            return json.load(file)

    def photo_to_vectors(self, img_name, detector_backend="mtcnn", normalization="Facenet"):
        """
        :return:
        """
        vectors = []

        try:
            vectors = deepfaces.represent(img_obj=img_name, model=self.model, enforce_detection=True,
                                          detector_backend=detector_backend,
                                          normalization=normalization)
            num_faces = len(vectors)
            if num_faces == 1:
                print(f"\nImage: {img_name}, found {num_faces} face...\n")
            else:
                print(f"\nImage: {img_name}, found {len(vectors)} faces...\n")
        except:
            print(f"\nFaces not found...")
        return vectors

    def verify_vectors(self, vector1, vector2, distance_metric="cosine"):
        """

        :param vector1:
        :param vector2:
        :param distance_metric:
        :return:
        """
        if distance_metric == 'cosine':
            distance = dst.findCosineDistance(vector1, vector2)
        elif distance_metric == 'euclidean':
            distance = dst.findEuclideanDistance(vector1, vector2)
        elif distance_metric == 'euclidean_l2':
            distance = dst.findEuclideanDistance(dst.l2_normalize(vector1),
                                                 dst.l2_normalize(vector2))
        else:
            raise ValueError("Invalid distance_metric passed - ", distance_metric)

        distance = np.float64(distance)
        threshold = dst.findThreshold(self.model_name, distance_metric)

        if distance <= threshold:
            identified = True
        else:
            identified = False

        resp_obj = {"verified": identified, "distance": distance, "max_threshold_to_verify": threshold,
                    "model": self.model_name, "similarity_metric": distance_metric}
        print(f"{resp_obj}\n")
        return identified

    def create_json_vectors(self):
        """
        :return: pass
        """
        json_faces = {"faces": []}
        for img_name in tqdm(os.listdir(self.photos_path)):
            json_struct = {"img_name": ''}
            vectors = self.photo_to_vectors(join(self.photos_path, img_name))

            json_struct["img_name"] = img_name
            json_struct["vectors"] = vectors
            json_faces["faces"].append(json_struct)

        self.save_json(json_faces, json_path=self.json_path)

    def selfie_vectors_compare(self):
        """
        :return:
        """
        photos_positive = []
        selfie_vectors = self.photo_to_vectors(self.selfie_path)
        if len(selfie_vectors) > 1 or len(selfie_vectors) == 0:
            print("Please, give selfie photo with 1 face...")
            return photos_positive
        else:
            selfie_vector = selfie_vectors[0]

        faces_json = self.read_json(self.json_path)

        selfie_name = self.selfie_path.split('/')[-1]
        print(f"selfie: {selfie_name}")

        for item in faces_json["faces"]:
            photo_name, vectors = item["img_name"], item["vectors"]
            for vector in vectors:
                if self.verify_vectors(vector, selfie_vector):
                    print(f"img_to_compare: {photo_name}, verified: True")
                    photos_positive.append(photo_name)
                    break
        return photos_positive

    def copy_positive_photos(self, photos):
        """
        :param photos:
        :return:
        """
        check_exist_path(self.result_path)
        for photo_name in tqdm(photos):
            try:
                shutil.copy(join(self.photos_path, photo_name), join(self.result_path, photo_name))
            except:
                shutil.copy(join(self.photos_path, photo_name), join(self.result_path, photo_name))
