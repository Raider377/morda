from mmocr.utils.ocr import MMOCR
from os.path import join
from tqdm import tqdm
from PIL import Image

import os
import json
import cv2
import numpy as np
import re


class TagRecognizer:
    def __init__(self, photo_path="zabeg"):
        """
        :param photo_path:
        """
        self.photo_path = photo_path
        self.ocr = MMOCR(det='MaskRCNN_CTW', recog='SAR')

    def get_predict(self, img_name):
        """
        :return:
        """
        results = self.ocr.readtext(join(self.photo_path, img_name), print_result=True, imshow=False, details=True)
        return results[0]['result']

    @staticmethod
    def save_json(data, json_name='tags'):
        """
        Saving json of ensemble faces data
        :param data:
        :param json_name:
        """
        with open(json_name + '.json', 'w') as f:
            json.dump(data, f, ensure_ascii=False)

    @staticmethod
    def read_json(json_name):
        """
        Read json of ensemble faces data
        :param json_name:
        :return: json
        """
        with open(json_name, "r") as f:
            return json.load(f)

    @staticmethod
    def get_idx_item(_json, find_item):
        """
        :return:
        """
        return next((idx for (idx, val) in enumerate(_json["tags"]) if val["tag"] == find_item), None)


if __name__ == '__main__':

    tagRecognizer = TagRecognizer(photo_path="data/zabeg")
    json_tags = {"tags": []}
    json_images = {"images": []}

    for f_n in tqdm(sorted(os.listdir(tagRecognizer.photo_path))):

        predict = tagRecognizer.get_predict(f_n)
        json_struct_img = {"image": f_n, "tags": []}

        # print(f"\n{predict}\n")
        for item in predict:
            json_struct = {"tag": '', "images": []}
            tag = re.sub('[^0-9]', '', item['text'])
            if tag.isdigit():
                print(f"tag={tag}, score={item['text_score']}")
                if tagRecognizer.get_idx_item(json_tags, tag) is None:

                    json_struct["tag"] = tag
                    json_struct["images"].append(f_n)
                    json_tags["tags"].append(json_struct)
                    print(f"\nAdded new tag\n")
                else:
                    idx_tag = tagRecognizer.get_idx_item(json_tags, tag)
                    json_tags["tags"][idx_tag]["images"].append(f_n)
                    print(f"\nAdded new img to an existing tag\n")

                json_struct_img["tags"].append(tag)

            json_images["images"].append(json_struct_img)

    tagRecognizer.save_json(json_tags)
    tagRecognizer.save_json(json_images, json_name='images')
