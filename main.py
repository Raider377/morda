from deepselfie import DeepSelfie
from yolo_image import YOLO
from thuman import Thuman

import tensorflow as tf
import numpy as np
import argparse
import random
import os


# config = tf.compat.v1.ConfigProto()
# config.gpu_options.allow_growth = True
# session = tf.compat.v1.InteractiveSession(config=config)


def get_available_devices():
    local_device_protos = tf.python.client.device_lib.list_local_devices()
    print(f"\n{[x.name for x in local_device_protos]}\n")


get_available_devices()
# tf.compat.v1.disable_eager_execution()

SEED = 66
os.environ['PYTHONHASHSEED'] = str(SEED)
os.environ['TF_CUDNN_DETERMINISTIC'] = '1'  # new flag present in tf 2.0+
random.seed(SEED)
np.random.seed(SEED)
tf.compat.v2.random.set_seed(SEED)


def check_exist_path(path):
    if not os.path.exists(path):
        os.mkdir(path)


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type=str, default="model-weights/YOLO_Face.h5",
                        help='path to model weights file')
    parser.add_argument('--anchors', type=str, default="cfg/yolo_anchors.txt",
                        help='path to anchor definitions')
    parser.add_argument('--classes', type=str, default="cfg/face_classes.txt",
                        help='path to class definitions')
    parser.add_argument('--score', type=float, default=0.5,
                        help='the score threshold')
    parser.add_argument('--iou', type=float, default=0.3,
                        help='the iou threshold')
    parser.add_argument('--img-size', type=list,
                        default=(832, 832), help='input image size')
    parser.add_argument('--photos', type=str, default="data/ДР Оля Social Club",
                        help='path to photo for vectorization')
    parser.add_argument('--video', type=str, default="data/1F-02_20210904-150000--20210904-160000.mp4",
                        help='path to video for processing')
    parser.add_argument('--video_save', type=str, default="data",
                        help='path to save video tracking')
    parser.add_argument('--video_json', type=str, default="data/video.json",
                        help='path to save faces json from video')
    parser.add_argument('--selfie', type=str, default="data/selfies/2021-09-29 13.46.47.jpg",
                        help='path to selfie for comparing')
    parser.add_argument('--result', type=str, default="data/result",
                        help='path to extract positives photos')
    parser.add_argument('--json', type=str, default="data/DeepSelfie.json",
                        help='path to save vectors json')
    return parser.parse_args()


if __name__ == '__main__':
    _args = get_args()
    thuman = Thuman(_args)
    thuman.video_process()

    # deepselfie = DeepSelfie(_args)
    #
    # # deepselfie.create_json_vectors()
    # result_photos = deepselfie.selfie_vectors_compare()
    # deepselfie.copy_positive_photos(result_photos)
