from requests.auth import HTTPBasicAuth
from os.path import join
from tqdm import tqdm

import os
import json
import requests


class TagParser:
    def __init__(self, photo_path="zabeg"):
        """
        :param photo_path:
        """
        self.url = 'https://app.nanonets.com/api/v2/OCR/Model/6854d528-efaf-468c-a708-8211f94615e8/LabelFile/'
        self.photo_path = photo_path

    def post_request(self, f_name):
        """

        :param f_name:
        :return:
        """
        data = {'file': open(join(self.photo_path, f_name), 'rb')}
        return requests.post(self.url, auth=HTTPBasicAuth('pTZqrB1a-TyRviRznY19X3HTxGHiBu9I', ''),
                             files=data)

    def get_response_json(self, f_name):
        """

        :param f_name:
        :return:
        """
        try:
            response = self.post_request(f_name)
        except Exception as err:
            print(err)
            response = self.post_request(f_name)
        return response.json()

    @staticmethod
    def save_json(data, json_name='tags'):
        """
        Saving json of ensemble faces data
        :param data:
        :param json_name:
        """
        with open(json_name + '.json', 'w') as f:
            json.dump(data, f, ensure_ascii=False)

    @staticmethod
    def read_json(json_name):
        """
        Read json of ensemble faces data
        :param json_name:
        :return: json
        """
        with open(json_name, "r") as f:
            return json.load(f)

    @staticmethod
    def get_idx_tag(_json, find_tag):
        """
        :return:
        """
        return next((idx for (idx, val) in enumerate(_json["tags"]) if val["tag"] == find_tag), None)


if __name__ == '__main__':

    tagParser = TagParser(photo_path="zabeg")
    json_tags = {"tags": []}

    for f_n in tqdm(sorted(os.listdir(tagParser.photo_path))):
        response_json = tagParser.get_response_json(f_n)

        if response_json is None or response_json['result'] is None:
            continue

        ocr_pred = response_json['result'][0]['prediction']
        img_name = response_json['result'][0]["input"]

        print(f"\n{ocr_pred}\n")
        for item in ocr_pred:
            json_struct = {"tag": '', "images": []}
            tag = item['ocr_text']
            if tagParser.get_idx_tag(json_tags, tag) is None:
                json_struct["tag"] = item['ocr_text']
                json_struct["images"].append(img_name)
                json_tags["tags"].append(json_struct)
                print(f"\nAdded new tag\n")
            else:
                idx_tag = tagParser.get_idx_tag(json_tags, tag)
                json_tags["tags"][idx_tag]["images"].append(img_name)
                print(f"\nAdded new img to an existing tag\n")
        print(json_tags)
    tagParser.save_json(json_tags)

    # test = tagParser.read_json('test.json')
    # print(tagParser.get_idx_tag(test, '1083'))
    # print(test["tags"])
    # print('1083' in test["tags"])
