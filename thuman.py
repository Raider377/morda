from deepface.commons import distance as dst
from deepselfie import DeepSelfie
from os.path import join, isfile
from tqdm import tqdm

import tensorflow as tf
import numpy as np
import deepfaces
import json
import time
import cv2
import os


def logging_time(func):
    def inner(*args, **kwargs):
        start = time.time()
        func(*args, **kwargs)
        end = time.time()
        printer()
        print(f"{func.__name__} time elapsed: {round(end - start, 2)} sec")

    return inner


def printer():
    print('_____________________________________________')


# TODO: added save frames to gif
class Thuman:
    def __init__(self, args):
        self.args = args
        self.video_path = args.video
        self.video_save = args.video_save
        self.json_video_path = args.video_json
        self.frame_counter = -1
        self.model_name = "Facenet"
        self.model = deepfaces.build_model(self.model_name)
        self.frame_space = 10

        # TODO load max id from json
        self._id = 1
        self.frame_names = []

    @staticmethod
    def save_json(data, json_path):
        """
        Save json file to disk
        :param data: json file for saving
        :param json_path: saving name
        :return: pass
        """
        with open(json_path, 'w') as file:
            json.dump(data, file, ensure_ascii=False)

    @staticmethod
    def read_json(json_path):
        """
        Read json file from disk
        :param json_path: name (included dir) for reading file
        :return: loaded json
        """
        with open(json_path, "r") as file:
            return json.load(file)

    def frame2vectors(self, frame, frame_name, detector_backend="mtcnn", normalization="Facenet"):
        """
        :return:
        """
        vectors = []
        regions = None
        try:
            vectors, regions = deepfaces.represent(img_obj=frame, model=self.model, enforce_detection=True,
                                                   detector_backend=detector_backend, return_regions=True,
                                                   normalization=normalization)
            num_faces = len(vectors)
            if num_faces == 1:
                print(f"\n{frame_name}, found {num_faces} face...")
            else:
                print(f"\n{frame_name}, found {len(vectors)} faces...")

        except:
            print("Faces not found...")
        printer()
        return vectors, regions

    def compare_vectors(self, vector1, vector2, distance_metric="cosine", print_result=False):
        """
        :param print_result:
        :param vector1:
        :param vector2:
        :param distance_metric:
        :return:
        """
        if distance_metric == 'cosine':
            distance = dst.findCosineDistance(vector1, vector2)
        elif distance_metric == 'euclidean':
            distance = dst.findEuclideanDistance(vector1, vector2)
        elif distance_metric == 'euclidean_l2':
            distance = dst.findEuclideanDistance(dst.l2_normalize(vector1),
                                                 dst.l2_normalize(vector2))
        else:
            raise ValueError("Invalid distance_metric passed - ", distance_metric)

        distance = np.float64(distance)
        threshold = dst.findThreshold(self.model_name, distance_metric)

        if threshold >= distance > 0 != distance:
            identified = True
        else:
            identified = False

        resp_obj = {"verified": identified, "distance": distance, "max_threshold_to_verify": threshold,
                    "model": self.model_name, "similarity_metric": distance_metric}
        if print_result:
            print(f"{resp_obj}\n")
        return identified, distance

    def check_exist_create_json(self, video_name):
        if not isfile(self.json_video_path):
            #     return self.read_json(self.json_video_path)
            # else:
            json_video = {"vectors": [], "video_name": video_name}
            self.save_json(json_video, json_path=self.json_video_path)

    def update_json(self, vectors, frame_name):
        """
        :param frame_name:
        :param vectors:
        :return:
        """
        distance_min = 1.01
        name = ''
        json_video = self.read_json(self.json_video_path)
        for vector in vectors:
            if len(json_video["vectors"]) == 0:
                json_video["vectors"].append({"name": self._id, "frame_name": frame_name, "vector": vector})
                self._id += 1
                self.frame_names.append(self._id)
            else:
                for item in json_video["vectors"]:
                    vec_for_comparison = item["vector"]
                    identify, distance = self.compare_vectors(vector, vec_for_comparison)
                    if identify and distance < distance_min:
                        distance_min = distance
                        name = item['name']
                if name == '':
                    print(f"{self._id} has entered...")
                    json_video["vectors"].append({"name": self._id, "frame_name": frame_name, "vector": vector})
                    self._id += 1
                    self.frame_names.append(self._id)
                else:
                    print(f"Person {name} finded in database, distance: {distance_min}...")
                    self.frame_names.append(name)

        self.save_json(json_video, self.json_video_path)

    @logging_time
    def video_process(self, write_video=True):
        """
        :return:
        """
        video_name = str.split(self.video_path, "/")[-1].split(".")[0]
        print(f"video name: {video_name}\n")
        self.check_exist_create_json(video_name)

        out = None
        vid = cv2.VideoCapture(self.video_path)
        if not vid.isOpened():
            raise IOError("Couldn't open video")

        # the video format and fps
        # video_fourcc = int(vid.get(cv2.CAP_PROP_FOURCC))
        video_fourcc = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
        video_fps = vid.get(cv2.CAP_PROP_FPS)

        # the size of the frames to write
        video_size = (int(vid.get(cv2.CAP_PROP_FRAME_WIDTH)),
                      int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT)))

        if write_video:
            out = cv2.VideoWriter(join(self.video_save, video_name + ".avi"),
                                  video_fourcc, video_fps, video_size)

        while True:
            ret, frame = vid.read()
            if not ret:
                break

            self.frame_counter += 1
            # if self.frame_counter % self.frame_space != 0:
            #     continue
            ###
            if self.frame_counter == 5000:
                break
            ###
            self.frame_names = []

            frame_bbox = frame.copy()
            frame_name = f"frame_{self.frame_counter}"
            vectors, regions = self.frame2vectors(frame, frame_name=frame_name)
            if vectors:
                self.update_json(vectors, frame_name)

            if regions is not None:
                for (x, y, w, h), name in zip(regions, self.frame_names):
                    cv2.rectangle(frame_bbox, (x, y), (x + w, y + h), (0, 255, 255), 2)
                    y1 = y - 15 if y - 15 > 15 else y + 15
                    cv2.putText(frame_bbox, str(name), (int(x), int(y1)), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)

            if write_video:
                out.write(frame_bbox)

        vid.release()
        out.release()
