from deepface import DeepFace
from os.path import join
from tensorflow.python.client import device_lib
from tqdm import tqdm
from timeit import default_timer as timer
from keras import backend as K
from PIL import ImageDraw, Image
from keras.models import load_model
from yolo.model import eval
import shutil
# from src.utils.preprocessor import preprocess_input
from deepface.commons import distance as dst
from ISR.models import RDN
from deepface.basemodels import Boosting

import cv2
import argparse
import colorsys
import tensorflow as tf
import os
import numpy as np
import json

ROOT_DIR = os.path.abspath("")
SELFIE = "selfies/2021-09-29 13.48.32.jpg"
TRUE_PHOTOS = "true_photos"
# PHOTOS = "zabeg"
# FACES_PHOTOS = "zabeg_faces"
# FACES_RES = "zabeg_faces_res"


PHOTOS = "ДР Оля Social Club"
FACES_PHOTOS = "ДР Оля Social Club_faces"
FACES_RES = "ДР Оля Social Club_faces_res"

# PHOTOS = "photos"
# FACES_PHOTOS = "faces"
# FACES_RES = "faces_res"

config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.compat.v1.InteractiveSession(config=config)


def get_available_devices():
    local_device_protos = device_lib.list_local_devices()
    print(f"\n{[x.name for x in local_device_protos]}\n")


get_available_devices()

tf.compat.v1.disable_eager_execution()


def check_exist_path(path):
    if not os.path.exists(path):
        os.mkdir(path)


check_exist_path(FACES_PHOTOS)
check_exist_path(FACES_RES)
check_exist_path(TRUE_PHOTOS)


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type=str, default='model-weights/YOLO_Face.h5',
                        help='path to model weights file')
    parser.add_argument('--anchors', type=str, default='cfg/yolo_anchors.txt',
                        help='path to anchor definitions')
    parser.add_argument('--classes', type=str, default='cfg/face_classes.txt',
                        help='path to class definitions')
    parser.add_argument('--score', type=float, default=0.5,
                        help='the score threshold')
    parser.add_argument('--iou', type=float, default=0.3,
                        help='the iou threshold')
    parser.add_argument('--img-size', type=list,
                        default=(832, 832), help='input image size')
    _args = parser.parse_args()
    return _args


class YOLO(object):
    def __init__(self, args):
        self.args = args
        self.model_path = args.model
        self.classes_path = args.classes
        self.anchors_path = args.anchors
        self.class_names = self._get_class()
        self.anchors = self._get_anchors()
        self.sess = tf.compat.v1.keras.backend.get_session()
        self.boxes, self.scores, self.classes = self._generate()
        self.model_image_size = args.img_size

    def _get_class(self):
        classes_path = os.path.expanduser(self.classes_path)
        with open(classes_path) as f:
            class_names = f.readlines()
        class_names = [c.strip() for c in class_names]
        return class_names

    def _get_anchors(self):
        anchors_path = os.path.expanduser(self.anchors_path)
        with open(anchors_path) as f:
            anchors = f.readline()
        anchors = [float(x) for x in anchors.split(',')]
        return np.array(anchors).reshape(-1, 2)

    def _generate(self):
        model_path = os.path.expanduser(self.model_path)
        assert model_path.endswith('.h5'), 'Keras model or weights must be a .h5 file'

        # load model, or construct model and load weights
        num_anchors = len(self.anchors)
        num_classes = len(self.class_names)
        try:
            self.yolo_model = load_model(model_path, compile=False)
        except Exception as err:
            print(err)
            # make sure model, anchors and classes match
            self.yolo_model.load_weights(self.model_path)
        else:
            # Mismatch between model and given anchor and class sizes
            assert self.yolo_model.layers[-1].output_shape[-1] == \
                   num_anchors / len(self.yolo_model.output) * (
                           num_classes + 5)
        self.yolo_model._make_predict_function()
        print('*** {} model, anchors, and classes loaded.'.format(model_path))

        # generate colors for drawing bounding boxes
        hsv_tuples = [(x / len(self.class_names), 1., 1.)
                      for x in range(len(self.class_names))]
        self.colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
        self.colors = list(
            map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)),
                self.colors))

        # shuffle colors to decorrelate adjacent classes.
        np.random.seed(102)
        np.random.shuffle(self.colors)
        np.random.seed(None)

        # generate output tensor targets for filtered bounding boxes.
        self.input_image_shape = K.placeholder(shape=(2,))
        boxes, scores, classes = eval(self.yolo_model.output, self.anchors,
                                      len(self.class_names),
                                      self.input_image_shape,
                                      score_threshold=self.args.score,
                                      iou_threshold=self.args.iou)
        return boxes, scores, classes

    def detect_image(self, image):
        def get_yolo_model_input(_image):
            if self.model_image_size != (None, None):
                assert self.model_image_size[0] % 32 == 0, 'Multiples of 32 required'
                assert self.model_image_size[1] % 32 == 0, 'Multiples of 32 required'
                boxed_image = letterbox_image(_image, tuple(
                    reversed(self.model_image_size)))
            else:
                new_image_size = (_image.width - (_image.width % 32),
                                  _image.height - (_image.height % 32))
                boxed_image = letterbox_image(_image, new_image_size)
            _image_data = np.array(boxed_image, dtype='float32')
            _image_data /= 255.
            # add batch dimension
            _image_data = np.expand_dims(_image_data, 0)
            return _image_data

        start_time = timer()

        image_data = get_yolo_model_input(image)

        out_boxes, out_scores, out_classes = self.sess.run(
            [self.boxes, self.scores, self.classes],
            feed_dict={
                self.yolo_model.input: image_data,
                self.input_image_shape: [image.size[1], image.size[0]],
                K.learning_phase(): 0
            })
        print('*** Found {} face(s) for this image'.format(len(out_boxes)))
        thickness = (image.size[0] + image.size[1]) // 400

        for i, c in reversed(list(enumerate(out_classes))):
            # predicted_class = self.class_names[c]
            box = out_boxes[i]
            # score = out_scores[i]
            # text = '{} {:.2f}'.format(predicted_class, score)
            draw = ImageDraw.Draw(image)

            top, left, bottom, right = box
            top = max(0, np.floor(top + 0.5).astype('int32'))
            left = max(0, np.floor(left + 0.5).astype('int32'))
            bottom = min(image.size[1], np.floor(bottom + 0.5).astype('int32'))
            right = min(image.size[0], np.floor(right + 0.5).astype('int32'))

            # print(text, (left, top), (right, bottom))

            for thk in range(thickness):
                draw.rectangle(
                    [left + thk, top + thk, right - thk, bottom - thk],
                    outline=(51, 178, 255), width=1)
            del draw

        end_time = timer()
        print('*** Processing time: {:.2f}ms'.format((end_time -
                                                      start_time) * 1000))
        return image, out_boxes

    def close_session(self):
        self.sess.close()


def letterbox_image(image, size):
    """Resize image with unchanged aspect ratio using padding"""
    img_width, img_height = image.size
    w, h = size
    scale = min(w / img_width, h / img_height)
    nw = int(img_width * scale)
    nh = int(img_height * scale)

    image = image.resize((nw, nh), Image.BICUBIC)
    new_image = Image.new('RGB', size, (128, 128, 128))
    new_image.paste(image, ((w - nw) // 2, (h - nh) // 2))
    return new_image


def detect_img(yolo):
    while True:
        img = input('*** Input image filename: ')
        try:
            image = Image.open(img)
        except Exception as err:
            print(err)
            if img == 'q' or img == 'Q':
                break
            else:
                print('*** Open Error! Try again!')
                continue
        else:
            res_image, _ = yolo.detect_image(image)
            res_image.show()
    yolo.close_session()


def save_face(image, locations, face_n, path_to_save_face=FACES_PHOTOS, margin=100):
    """

    :param path_to_save_face:
    :param image:
    :param locations:
    :param face_n:
    :param margin:
    :return:
    """
    top, left, bottom, right = locations

    img_h, img_w, _ = np.shape(image)
    top = max(int(top - margin), 0)
    left = max(int(left - margin), 0)
    bottom = min(int(bottom + margin), img_h - 1)
    right = min(int(right + margin), img_w - 1)
    face_image = image[top:bottom, left:right]
    face_image = cv2.cvtColor(face_image, cv2.COLOR_BGR2RGB)

    # face_image = cv2.resize(src=face_image, dsize=(128, 128), interpolation=cv2.INTER_LINEAR)
    cv2.imwrite(join(path_to_save_face, face_n + '.jpg'), face_image, [cv2.IMWRITE_JPEG_QUALITY, 100])


def save_faces(dir_imgs=PHOTOS):
    """

    :param dir_imgs:
    :return:
    """
    args = get_args()
    yolo = YOLO(args)
    for img_name in tqdm(sorted(os.listdir(dir_imgs))):

        img = Image.open(join(dir_imgs, img_name))
        input_img = np.array(img.copy())

        res, boxes = yolo.detect_image(img)
        idx = 0
        for box in boxes:
            idx += 1
            save_face(input_img, box, img_name.split('.')[0] + '_' + str(idx))


def verify_photo(dir_photo, img_n_to_compare, detector_backend="mtcnn",
                 distance_metric="cosine", normalization="base", model_name="Ensemble"):
    try:
        result = DeepFace.verify(SELFIE, join(dir_photo, img_n_to_compare), detector_backend=detector_backend,
                                 distance_metric=distance_metric, normalization=normalization,
                                 model_name=model_name)
    except:
        result = DeepFace.verify(SELFIE, join(dir_photo, img_n_to_compare), detector_backend=detector_backend,
                                 distance_metric=distance_metric, normalization=normalization,
                                 model_name=model_name, enforce_detection=False)
    # if result['verified']:
    print('\n-------')
    print(img_n_to_compare)
    print(result)
    print('-------\n')


def verify_photos(dir_photos="faces"):
    """

    :param dir_photos:
    :return:
    """
    for img_name in tqdm(sorted(os.listdir(dir_photos))):
        verify_photo(dir_photos, img_name)


def save_json(data, json_name):
    """
    Json saver
    :param data:
    :param json_name:
    :return:
    """
    with open(json_name + '.json', 'w') as f:
        json.dump(data, f, ensure_ascii=False)


def read_json(json_name):
    with open(json_name, "r") as f:
        return json.load(f)


def get_vector(img_n, dir_img, model_name="VGG-Face", detector_backend="mtcnn", normalization="base"):
    """

    :param dir_img:
    :param img_n:
    :param model_name:
    :param detector_backend:
    :param normalization:
    :return:
    """
    try:
        result = DeepFace.represent(join(dir_img, img_n), model_name=model_name, detector_backend=detector_backend,
                                    normalization=normalization)
    except:
        result = DeepFace.represent(join(dir_img, img_n), model_name=model_name, detector_backend=detector_backend,
                                    normalization=normalization,
                                    enforce_detection=False)
    print('\n-------')
    print(img_n)
    print(result)
    print('-------\n')

    return result


def get_vectors_json(dir_faces, save=True):
    """

    :param dir_faces:
    :param save:
    :return:
    """
    json_faces = {"faces": []}

    for img_name in tqdm(sorted(os.listdir(dir_faces))):
        json_struct = {"img_name": '', "face_vector": []}

        vector = get_vector(img_name, dir_faces)

        json_struct["img_name"] = img_name
        json_struct["face_vector"] += vector
        json_faces["faces"].append(json_struct)

    if save:
        save_json(data=json_faces, json_name="faces")
    else:
        return json_faces


def verify_vectors_faces(vector1, vector2, distance_metric='cosine', model_name="VGG-Face"):
    """

    :param vector1:
    :param vector2:
    :param distance_metric:
    :param model_name:
    :return:
    """
    if distance_metric == 'cosine':
        distance = dst.findCosineDistance(vector1, vector2)
    elif distance_metric == 'euclidean':
        distance = dst.findEuclideanDistance(vector1, vector2)
    elif distance_metric == 'euclidean_l2':
        distance = dst.findEuclideanDistance(dst.l2_normalize(vector1),
                                             dst.l2_normalize(vector2))
    else:
        raise ValueError("Invalid distance_metric passed - ", distance_metric)

    distance = np.float64(distance)

    threshold = dst.findThreshold(model_name, distance_metric)

    if distance <= threshold:
        identified = True
    else:
        identified = False

    resp_obj = {"verified": identified, "distance": distance, "max_threshold_to_verify": threshold,
                "model": model_name, "similarity_metric": distance_metric}
    print(f"{resp_obj}\n")

    return identified


def selfie_vectors_compare():
    selfie_name, vector_selfie = SELFIE.split('/')[-1], get_vector(SELFIE.split('/')[-1], SELFIE.split('/')[0])

    print(f"selfie_name: {selfie_name}, vector_selfie: {vector_selfie}")
    faces_json = read_json('faces.json')
    faces_true = []
    for item in faces_json["faces"]:

        img_to_compare, vector_to_compare = item["img_name"], item["face_vector"]
        print(f"img_to_compare: {img_to_compare}, vector_to_compare: {vector_to_compare}")

        if verify_vectors_faces(vector_selfie, vector_to_compare):
            faces_true.append(img_to_compare)

    return faces_true


def increase_resolution(dir_imgs):
    """
    :param dir_imgs:
    :return:
    """
    print("\nIncrease faces resolution...\n")
    try:
        rdn = RDN(weights='psnr-small')
        for img_n in tqdm(sorted(os.listdir(dir_imgs))):
            img = Image.open(join(dir_imgs, img_n))
            sr_img = rdn.predict(np.array(img))
            Image.fromarray(sr_img).save(join(FACES_RES, img_n))
    except Exception as err:
        print(err)
        print("All files have been saved successfully...\n")


class Ensemble:

    def __init__(self):
        self.model_names = ["VGG-Face", "Facenet", "OpenFace", "DeepFace"]
        self.models = Boosting.loadModel()
        self.model_name = "Ensemble"
        self.normalization = "base"
        self.metrics = ["cosine", "euclidean", "euclidean_l2"]
        self.boosted_tree = Boosting.build_gbm()

    def get_ensemble_vector(self, img_n, dir_img):
        """

        :param img_n:
        :param dir_img:
        :return:
        """
        ensemble_vec = []
        for i in self.model_names:
            custom_model = self.models[i]
            try:
                vec = DeepFace.represent(join(dir_img, img_n), model=custom_model, model_name=self.model_name,
                                         normalization=self.normalization)
            except:
                vec = DeepFace.represent(join(dir_img, img_n), model=custom_model, model_name=self.model_name,
                                         normalization=self.normalization,
                                         enforce_detection=False)

            ensemble_vec.append(vec)
        return ensemble_vec

    def get_custom_vector(self, img_n, dir_img, model):
        """
        :param img_n:
        :param dir_img:
        :param model:
        :return:
        """
        try:
            vec = DeepFace.represent(join(dir_img, img_n), model=model, model_name=self.model_name,
                                     normalization=self.normalization)
        except:
            vec = DeepFace.represent(join(dir_img, img_n), model=model, model_name=self.model_name,
                                     normalization=self.normalization,
                                     enforce_detection=False)
        return vec

    @staticmethod
    def save_json(data, json_name):
        """
        Saving json of ensemble faces data
        :param data:
        :param json_name:
        """
        with open(json_name + '.json', 'w') as f:
            json.dump(data, f, ensure_ascii=False)

    @staticmethod
    def read_json(json_name):
        """
        Read json of ensemble faces data
        :param json_name:
        :return: json
        """
        with open(json_name, "r") as f:
            return json.load(f)

    def save_ensemble_json(self, dir_faces):
        """

        :param dir_faces:
        :return:
        """
        json_faces = {"faces": []}

        for img_name in tqdm(sorted(os.listdir(dir_faces))):
            json_struct = {"img_name": '', "VGG-Face": [], "Facenet": [], "OpenFace": [], "DeepFace": []}

            vector = self.get_ensemble_vector(img_name, dir_faces)

            json_struct["img_name"] = img_name
            json_struct["VGG-Face"] += vector[0]
            json_struct["Facenet"] += vector[1]
            json_struct["OpenFace"] += vector[2]
            json_struct["DeepFace"] += vector[3]
            json_faces["faces"].append(json_struct)

        self.save_json(data=json_faces, json_name="faces_ensemble")

    def compare_selfie_ensemble(self, selfie, json_name='faces_ensemble.json'):
        """
        :return:
        """
        selfie_name = selfie.split('/')[-1]
        selfie_path = selfie.split('/')[0]
        print(f"selfie_name: {selfie_name}")

        yolo = YOLO(get_args())
        img = Image.open(selfie)
        input_img = np.array(img.copy())
        res, boxes = yolo.detect_image(img)
        if len(boxes) > 1:
            print("\nPlease give selfie image with one face...\n")
            return
        else:
            new_selfie_name = selfie_name.split('.')[0] + '_face'
            new_selfie_path = "selfie_faces"
            for box in boxes:
                save_face(input_img, box, new_selfie_name, path_to_save_face=new_selfie_path)

        faces_ensemble_json = read_json(json_name)
        faces_ensemble_true = []

        vectors_selfies = [self.get_custom_vector(new_selfie_name + '.jpg', new_selfie_path, self.models[name]) for name
                           in self.model_names]

        for item in tqdm(faces_ensemble_json["faces"]):
            ensemble_features = []
            img_name = item["img_name"]

            for idx, val in enumerate(self.model_names):

                vector_selfie = vectors_selfies[idx]
                vector_this_model = item[val]

                for j in self.metrics:

                    if j == 'cosine':
                        distance = dst.findCosineDistance(vector_selfie, vector_this_model)
                    elif j == 'euclidean':
                        distance = dst.findEuclideanDistance(vector_selfie, vector_this_model)
                    elif j == 'euclidean_l2':
                        distance = dst.findEuclideanDistance(dst.l2_normalize(vector_selfie),
                                                             dst.l2_normalize(vector_this_model))
                    else:
                        raise ValueError("Invalid distance_metric passed")
                    distance = np.float64(distance)

                    if val == 'OpenFace' and j == 'euclidean':
                        continue
                    else:
                        ensemble_features.append(distance)

            prediction = self.boosted_tree.predict(np.expand_dims(np.array(ensemble_features), axis=0))[0]

            verified = np.argmax(prediction) == 1
            score = prediction[np.argmax(prediction)]

            resp_obj = {
                "verified": verified,
                "score": score,
                "distance": ensemble_features,
                "model": self.model_names,
                "similarity_metric": self.metrics
            }
            print(f"\n{resp_obj}\n")
            if resp_obj["verified"] and score > 0.9:
                faces_ensemble_true.append(img_name)

        return faces_ensemble_true

    @staticmethod
    def copy_trues_photo(faces_ensemble_true, path_from, path_to):
        for f_n in faces_ensemble_true:
            fn_split = f_n.split('.')[0]
            try:
                f1 = fn_split[0: fn_split.rfind('_')] + '.jpg'
                shutil.copy(join(path_from, f1), join(path_to, f1))
            except:
                f2 = fn_split[0: fn_split.rfind('_')] + '.JPG'
                shutil.copy(join(path_from, f2), join(path_to, f2))


if __name__ == '__main__':

    # save_faces(dir_imgs=PHOTOS)
    # increase_resolution(dir_imgs=FACES_PHOTOS)

    ensemble = Ensemble()
    # ensemble.save_ensemble_json(dir_faces=FACES_PHOTOS)
    faces_true_names = ensemble.compare_selfie_ensemble(selfie=SELFIE,
                                                        json_name="faces_ensemble.json")

    if faces_true_names:
        ensemble.copy_trues_photo(faces_true_names, path_from=PHOTOS, path_to=TRUE_PHOTOS)

    # for self_name in tqdm(os.listdir("selfies")):
    #     selfie_dir = join("selfies", self_name)
    #     faces_true_names = ensemble.compare_selfie_ensemble(selfie=selfie_dir,
    #                                                         json_name="faces_ensemble_dr.json")
    #     path_to_copy = join("result", self_name)
    #     check_exist_path(path_to_copy)
    #     ensemble.copy_trues_photo(faces_true_names, path_from=PHOTOS, path_to=path_to_copy)
